# Change log #
### v0.4 ###
* add isInList / isEmpty
* add demo at Source.cpp
* add comments
### V0.3 ###
* add headPop / tailPop
### V0.2 ###
* add headPush / tailPush
### V0.1 ###
* starter files
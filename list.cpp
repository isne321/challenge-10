#include <iostream>
#include <windows.h>
#include "list.h"

using namespace std;

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::headPush(int data) {
	head = new Node(data, head);
}

void List::tailPush(int data) {
	Node *tmp = head;
	while (true) { //always true
		if (tmp->next == 0) { //when temp pass to last node
			tail = new Node(data, 0);
			tmp->next = tail;
			break;
		}
		else {
			tmp = tmp->next;
		}
	}
}

int List::headPop() {

	Node *tmp = head->next; //temp is second node
	delete head; //delete first one
	head = tmp; //makes head to second node
	return head->info;

}

int List::tailPop() {
	Node *tmp = head;
	while (true) {
		if (tmp->next->next == 0) { //next to next is last node
			tail = tmp;
			tail->next = 0;
			tmp = tmp->next;
			delete tmp;
			return tail->info;
		}
		else {
			tmp = tmp->next;
		}
	}
}

void List::deleteNode(int search) {

	Node *tmp = head;
	Node *tmp_2 = head;

	while (true) {
		if (tmp->info == search) {
			if (tmp == tail) { //if the value is last node
				List::tailPop();
				return;
			}
			else if (tmp == head) {
				List::headPop(); //if the value is first node
				return;
			}
			else {
				tmp_2->next = tmp->next; //pass
				delete tmp;
				tmp = tmp_2;
				return;

			}
		}

		if (tmp != head) {
			tmp_2 = tmp_2->next;
		}

		if (tmp->next == 0) {
			return;
		}
		tmp = tmp->next;

	}
}

bool List::isInList(int search) {
	Node *tmp = head;
	while (true) {
		if (tmp->info == search) {
			return true;
		}
		tmp = tmp->next;
		if (tmp == 0) { //when passed to last node -> break
			break;
		}
	}

	return false;
}